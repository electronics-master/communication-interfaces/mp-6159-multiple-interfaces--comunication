/*
    Webserver - Main script
    ITCR: II QUARTER 2019
    By: Luis Leon & Emmanuel Madrigal

    Requirements: NodeJS 10, Express
*/

const WEB_SERVER_PORT = 3333;

/*
    Express JS set up
*/
var express = require('express');
var app = express();
var server = require('http').Server(app);
server.listen(WEB_SERVER_PORT); 
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
console.log("# HTTP Server: Successfully up");
console.log("# HTTP Server: Listening on: http://localhost:3333");

const events = require('events');
var communication_event = new events.EventEmitter();

/*
    Back-end routes
*/
const port_manager = require('./back-end/ports_manager.js');
port_manager(communication_event);
require("./back-end/get_trace_points.js").load(app, communication_event);



/*
    Front-end routes
*/
require("./back-end/site.js")(app);
