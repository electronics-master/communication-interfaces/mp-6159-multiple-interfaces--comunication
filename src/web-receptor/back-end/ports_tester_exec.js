/*
    Webserver - Ports tester - Debug Only
    ITCR: II QUARTER 2019
    By: Luis Leon & Emmanuel Madrigal
*/

const port_manager = require('./ports_manager.js');
const events = require('events');
var communication_event = new events.EventEmitter();

communication_event.on("new_data", function(data){
  /* FIXME - Add logic here */
  console.log("Received: ", data.toString());
});

port_manager(communication_event);

