/*
    Webserver - Ports manager
    ITCR: II QUARTER 2019
    By: Luis Leon & Emmanuel Madrigal
*/

var net = require('net');

module.exports = function(event) {
  /* Create TCP server */
  var server = net.createServer(function(socket) {
    socket.pipe(socket);
    socket.on('data', function(data){
      event.emit("new_data", data);
    });
  });

  /* Listen with `nc 127.0.0.1 1337` */
  server.listen(1337);
  
}




