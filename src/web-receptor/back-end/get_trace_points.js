/*
    Webserver - Dummy point generator
    ITCR: II QUARTER 2019
    By: Luis Leon & Emmanuel Madrigal
*/

/* FIXME - Remove these placeholders */
var points_buffer = [];

function load(app, com_event){
  trace_API(app, com_event);
}

function trace_API(app, com_event)
{
    app.get("/get_trace_points", function(req,res){
        var result = {
          error: 0,
          results: points_buffer
        };
        res.json(result);
    });
    app.get("/refresh_points", function(req,res){
      /* The route received n as the number of points to load */
      var result = {
        error: 0,
        results: points_buffer
      };
      res.json(result);

    });
    com_event.on("new_data", function(data){
      console.log("Received: ", data.toString());
      points_buffer.push({_id:null, location:JSON.parse(data.toString())});
    });
}


module.exports = {
  load : load
};