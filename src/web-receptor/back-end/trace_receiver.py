# Trace receiver
# ITCR: II QUARTER 2019
# By: Luis Leon & Emmanuel Madrigal

import json
import random
import socket
import time

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 1337        # The port used by the server

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_address = (HOST, PORT)
sock.connect(server_address)

# Generate dummy coordinates
def new_random_coordinate():
  coordinates = {"lat":0, "lon":0}
  coordinates["lat"] = 9 + random.random()
  coordinates["lon"] = -83 + random.random()
  return coordinates

count = 0

while 1:
  time.sleep(1)
  message = json.dumps(new_random_coordinate())
  sock.sendall(message.encode())
  count += 1
  print(message)
