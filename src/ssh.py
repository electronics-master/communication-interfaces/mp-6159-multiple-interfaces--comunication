#!/usr/bin/python3

import paramiko
import os
import csv
import json

#HOST_IP = "10.9.9.139"
HOST_IP = "192.168.0.112"
PORT = 2222

USERNAME = "root"
PASSWORD = "admin"

FILE_LOCATION = "/storage/emulated/0/Android/data/com.mendhak.gpslogger/files/"
FILE = "20190730.csv"

COORD_INTEGERS = 0
COORD_FRACTION = 12
COORD_LEFT = -83.5
COORD_BUTTOM = 9.5

COORDINATE_SHIFT = (2 ** (COORD_FRACTION - COORD_INTEGERS))
PACKAGE_SHIFT = (2 ** (COORD_FRACTION + COORD_INTEGERS))

THIRD_BYTE_SHIFT = (2 ** 16)
SECOND_BYTE_SHIFT = (2 ** 8)



def get_coordinates():
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    conn = client.connect(
        HOST_IP,
        username=USERNAME,
        password=PASSWORD,
        port=PORT)

    ftp_client = client.open_sftp()

    lat_idx = -1
    lon_idx = -1
    coord_list = []

    with ftp_client.file(os.path.join(FILE_LOCATION, FILE)) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                for i in range(len(row)):
                    if row[i] == "lat":
                        lat_idx = i
                    elif row[i] == "lon":
                        lon_idx = i
                    line_count += 1
            else:
                coord_list.append((row[lat_idx], row[lon_idx]))
                line_count += 1
        print("Processed", line_count," lines.")

    ftp_client.close()
    client.close()

    return coord_list


def bytes_to_coordinates(package, sock):
    output = (ord(package[0]) * THIRD_BYTE_SHIFT) + \
        (ord(package[1]) * SECOND_BYTE_SHIFT) + ord(package[2])

    lat = ((output & 0xFFF000) / PACKAGE_SHIFT) / \
        COORDINATE_SHIFT + COORD_BUTTOM
    lon = COORD_LEFT - (output & 0xFFF) / COORDINATE_SHIFT

    coordinates = {"lat":lat, "lon":lon}
    message = json.dumps(coordinates)
    sock.sendall(message.encode())


def coord_to_bytes(coordinates):
    coord_bytes = []

    for coord in coordinates:
        # Get coordinates as a difference from the corner
        lat = (float(coord[0]) - COORD_BUTTOM)
        lon = (COORD_LEFT - float(coord[1]))

        # Shift coordinates to Q-1.13
        lat = lat * COORDINATE_SHIFT
        lon = lon * COORDINATE_SHIFT

        # Add to a single package
        package = int(lat) * (PACKAGE_SHIFT) + int(lon)
        package = package.to_bytes(3, byteorder='big')

        coord_bytes.append(package)

    return coord_bytes


if __name__ == "__main__":
    coord_list = get_coordinates()
