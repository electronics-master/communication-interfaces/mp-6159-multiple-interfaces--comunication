#!/bin/python3

import codecs
import serial
import time
import crc8
import sys
import socket

import ssh

HEADER_MASK = 0xFC
HEADER_BYTE = 0xFC
TIMEOUT = 1  # s

DELAY_VALUES = [0.010, 0.025, 0.050, 0.100, 0.250, 0.500, 1, 2, 5]


# Create a TCP/IP socket
SERVER_IP = "127.0.0.1"
SERVER_PORT = 1337

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

def config_ser():
    port = input("Write the serial port: ")
    
    ser = serial.Serial("/dev/ttyUSB" + port, timeout=0.1)
    print(ser.name)

    packet = bytearray()

    print("-- Starting setup --")

    # Setup - Baudrate to 9600
    packet.append(163)
    packet.append(58)
    packet.append(2)
    ser.write(packet)
    packet = bytearray()

    # Setup - Channel - 20
    packet.append(167)
    packet.append(122)
    packet.append(20)
    ser.write(packet)
    packet = bytearray()

    # Setup - Module ID 11
    packet.append(169)
    packet.append(154)
    packet.append(1)
    packet.append(1)
    ser.write(packet)
    packet = bytearray()

    # Setup - TX Power
    packet.append(171)
    packet.append(186)
    packet.append(0)
    ser.write(packet)
    packet = bytearray()

    # Setup - Read conf
    packet.append(166)
    packet.append(106)
    ser.write(packet)
    packet = bytearray()

    return ser


def get_crc(header, msg):
    hash = crc8.crc8()
    hash.update(bytes([header]))
    hash.update(bytes([msg]))
    return int.from_bytes(hash.digest(), byteorder='little')


def send_msg(ser, c, sequence_id):
    msg_buffer = []

    send_msg = []
    rcv_msg = []
    acknowledge = False

    msg = []

    header = HEADER_BYTE | sequence_id
    # Add header
    msg.append(header)
    # Add message
    msg.append(c)
    # Add checksum
    msg.append(get_crc(msg[0], msg[1]))
    send_msg = msg

    ser.write(bytearray(msg))
    ts = time.time()

    # Wait for acknowledge
    while((time.time() - ts) < TIMEOUT and not acknowledge):
        msg = ser.readline()
        # Check if a message was received
        if(msg != b''):
            for c in msg:
                msg_buffer.append(c)

            # Get individual bytes
            for i in range(len(msg_buffer)):
                # Look for header
                if msg_buffer[i] == header:
                    # Check if received a complete message
                    if((len(msg_buffer) - i) >= 3):
                        # Calculate the crc
                        byte_crc = get_crc(header, msg_buffer[i + 1])
                        # Get crc from transmitted msg
                        msg_crc = msg_buffer[i + 2]

                        # Check crc
                        if byte_crc == msg_crc:
                            rcv_msg.append(msg_buffer[i])
                            rcv_msg.append(msg_buffer[i + 1])
                            rcv_msg.append(msg_buffer[i + 2])

                            if rcv_msg == send_msg:
                                acknowledge = True
                                break
                    else:
                        for c in msg:
                            msg_buffer.append(c)

    return acknowledge


def write_loop(ser, coord_bytes):
    sequence = 0
    for package in coord_bytes:
        # Separate message in single byte package
        for c in package:
            acknowledge = False
            retransmit_idx = 0

            while(not acknowledge):
                acknowledge = send_msg(ser, c, sequence)
                if(not acknowledge):
                    time.sleep(DELAY_VALUES[retransmit_idx])
                    retransmit_idx += 1
                    if(retransmit_idx == len(DELAY_VALUES)):
                        retransmit_idx = 0

            # Get id for current message
            sequence = sequence + 1
            if(sequence > 3):
                sequence = 0


def read_loop(ser):
    msg_buffer = []
    last_send = 0

    output_msg = ""

    msg_byte = 0
    crc_byte = 0

    this_msg = []
    return_msg = []

    while(1):
        msg = ser.readline()
        # Check if a message was received
        if(msg != b''):
            for c in msg:
                msg_buffer.append(c)

            # Get individual bytes
            for i in range(len(msg_buffer)):
                # Look for header
                if (msg_buffer[i] & HEADER_MASK) == HEADER_MASK:

                    # Check if received a complete message
                    if((len(msg_buffer) - i) >= 3):
                        # Save this message to internal variables
                        this_msg = []
                        this_msg.append(msg_buffer[i])
                        this_msg.append(msg_buffer[i + 1])
                        this_msg.append(msg_buffer[i + 2])

                        # If the message is repeated, resend
                        # acknowledge
                        if(this_msg == return_msg):
                            ser.write(bytearray(this_msg))
                            # Skip until next possible message
                            i = i + 2
                            # Update processed bytes counter
                            last_send = i + 1
                            continue

                        # Calculate the crc
                        byte_crc = get_crc(msg_buffer[i], msg_buffer[i + 1])

                        # Get crc from transmitted msg
                        msg_crc = msg_buffer[i + 2]

                        # Check crc
                        if byte_crc == msg_crc:
                            # Append received byte
                            output_msg += chr(msg_buffer[i + 1])

                            # Send msg back as acknowledgement
                            return_msg = []
                            # Add header
                            return_msg.append(msg_buffer[i])
                            # Add message
                            return_msg.append(msg_buffer[i + 1])
                            # Add checksum
                            return_msg.append(msg_buffer[i + 2])

                            # Send message
                            ser.write(bytearray(return_msg))

                            # Skip until next possible message
                            i = i + 2
                            # Update processed bytes counter
                            last_send = i + 1
                    else:
                        for c in msg:
                            msg_buffer.append(c)

            # Remove processed bytes
            msg_buffer = msg_buffer[last_send:]
            last_send = 0

            if output_msg != "":
                if(len(output_msg) == 3):
                    ssh.bytes_to_coordinates(output_msg, sock)
                    output_msg = ""


if __name__ == "__main__":
    ser = config_ser()

    # Usage
    print("-- Terminal started --")

    # indata = input(">> ")
    if len(sys.argv) > 1 and sys.argv[1] == 'r':
      indata = sys.argv[1]
      server_address = (SERVER_IP, SERVER_PORT)
      sock.connect(server_address)
    else:
      indata = "s"

    if indata == "s":
        coord_list = ssh.get_coordinates()
        coord_bytes = ssh.coord_to_bytes(coord_list)
        write_loop(ser, coord_bytes)
    elif indata == "r":
        read_loop(ser)

    ser.close()
